# Rapatrier les données de staging en local

Prérequis :
- Avoir `psql` et `pg_dump` installés sur votre machine
- Connaitre le mot de passe de l'utilisateur *falco* sur staging

> Pour vérifier la bonne installation de ``psql`` et `pg_dump` lancer les commandes ci-dessous :
> ```bash
> ➜ psql --version
>   psql (PostgreSQL) 13.1
>```
>```bash
> ➜ pg_dump --version
>   pg_dump (PostgreSQL) 13.1
>```

> *PS : Il est également possible de le faire depuis la machine docker, 
néanmoins pour que les commandes ci-dessous fonctionne à l'identitique, 
nous vous recommandons de le faire depuis votre postal local, donc d'installer les pré-requis*

## Comment exporter les données provenant de staging

Pour effectuer un dump de la base de staging il suffit de prendre comme exemple cette commande :

```bash
pg_dump --file=/{your_path}.sql --dbname=falco-db --schema=public --username=falco --host=staging-db-1.infra.appenin.fr --port=5432 --clean
```
- dbname = Nom de la DB à exporter
- host = Nom de l'hôte contenant la DB
- your_path = Le chemin de destination du fichier (ex: `pg_dump --file=/Users/rubendesert/DUMP/staging-dump.sql`)
- username = Correspond à l'owner de la DB à exporter
- port = numéro du port de la BD

Le CLI vous demandera un mot de passe pour se connecter. N'hésitez pas à demander à votre équipe pour obtenir les accès.

Une fois cette action effectuée vous devriez obtenir un fichier .sql à l'endroit précisé. Ce sera ce fichier que nous mettrons dans notre base de donnée locale.

## Comment importer les données exportées en local

> Attention cette manipulation vous fera perdre vos données en local

Pour effectuer la restauration des données que vous venez d'exporter sur votre machine il vous suffit de taper cette commande :

```bash
psql --file=/{your_path}.sql --dbname=test --username=test --host=localhost --port=54334
```

- dbname = Nom de la DB en local (par défaut: test)
- username = Nom de l'utilisateur de la DB en local (par défaut: test)
- host = localhost
- port = numéro du port de la BD (par défaut: 54334)

> :warning:
> **Il est possible que vous ayez des erreurs de ce type :**
>
> `sql:477: ERROR:  role "falco" does not exist`
>
> Ceci est dû au fait que PostgresSql tente d'éditer l'owner de votre base avec un utilisateur (falco) qui n'est pas présent dans votre base local. **Ceci n'est pas bloquant.**
--

Vérifier que les données ont bien été importées dans votre base local
