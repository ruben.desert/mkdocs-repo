# Code review guidelines

## Qu’est-ce qu’une code review ? 🤔
Après avoir développé une fonctionnalité (ou corrigé un bug), `commit/push` une nouvelle branche  dans le dépôt du projet sur GitLab et créé la `merge request (MR)` correspondante, un autre développeur doit effectuer une relecture du code pour approuver les modifications. Cette étape est essentielle avant de `merge` ce code sur la branche principale du projet.<br/>C'est ce qu'on appelle la **`code review`**.

## Pourquoi faire des codes review ? 🤨
1. 🙌 Améliorer la qualité du code
1. 🏆 S’assurer que les standards de développement sont respectés
1. 🏗 Valider l'implémentation choisie
1. 🐞 Trouver des potentiels bugs avant qu’ils ne soient mis en production
1. 👨‍🎓 Apprendre et partager le savoir peu importe son niveau ! 

## Comment faire une code review ? 🧐
### 1. 👍 Quand passer une MR en code review
- 👮 Sauf exception, une MR doit toujours ajouter un test, ou modifier un test existant.
- ✨ Formater le titre `[FEATURE|TECH|BUGFIX] #(numéro du ticket) décrire la MR en 1 phrase`
  - `FEATURE`: ajout d'une nouvelle fonctionnalité
  - `BUG`: correction d'un bug
  - `TECH`: amélioration du projet, sans ajout de nouvelle fonctionnalité
- ✅ Vérifier que les tests de la CI passent
- 🧑‍🏫 Expliquer/commenter vos choix, surtout si on a besoin de contexte pour comprendre (Ex: [front](https://gitlab.com/appenin/falco-front/-/merge_requests/221) et [back](https://gitlab.com/appenin/falco-api/-/merge_requests/307))
- 📚 Relire votre code : est-ce que vous auriez envie de lire ce code si c'était quelqu’un d'autre qui l’avait fait ?
- ✂️ Il est accepté de découper une MR en plusieurs petites MR si vous estimez que ça facilitera la code review (plus la MR est grosse, plus il y a de risques que des erreurs passent)
- 🧱 S’assurer que la MR est “non cassante”. C’est-à-dire que la MR se suffit à elle même et peut être merge sur la branche master sans effets de bords. Dis autrement, chaque MR doit pouvoir partir en production.

### 2. 🛠 Vérifier s’il y a des MR en attente d’une code review (Difficile d’en faire une s’il n’y en a pas 😛)
-   Vérifier sur le channel slack **`#tech_mr`** s’il y en a
-   Vérifier dans le **[boards Appenin](https://gitlab.com/groups/appenin/-/boards)** s’il y a des tickets en dev review

### 3. ⏱ Trouver le temps de faire une code review
- 🐣 Avant de commencer un nouveau ticket
- ☕️ En début de journée
- 👋 Signaler au daily lorsqu’une MR a besoin d’une review urgente
```
💡 Tips : Ne pas hésiter à demander au créateur/reviewer s’il est disponible
pour faire une code review ensemble !
```
### 4. 📚 Prendre connaissance de la tâche
- 📖 Lire le ticket lié à la MR, afin de comprendre le contexte et le comportement souhaité
- 🗣 Ne pas hésiter à demander au Product Owner ou au développeur s’il manque des informations
- 🤠 Lire les tests est une manière de comprendre le comportement attendu
```
💡 Tips : En tant que créateur de MR, n'hésites pas à ajouter des captures d'écran / gifs
de la fonctionnalité afin que le reviewer puisse comprendre plus facilement
ce qui est attendu
```
### 5. 📥 Récupérer la branche distante sur votre ordinateur
- 🕹 Jouer avec la nouvelle fonctionnalité
- 👍 Valider que le comportement souhaité est le bon
- ✨ Valider que le design est respecté et qu’il n’y a pas de fautes d’orthographe
- 🌍 En français et en anglais
 ```   
💡 Tips : En tant que Reviewer, N’hésites pas à faire des retours si l'expérience
utilisateur (ex: un bouton pas facile à utiliser, un texte pas facile à lire etc…).
Nous sommes également de potentiels clients, si quelque chose nous dérange,
il est fort probable que ça dérange également une autre personne.
```
### 6. 💻 Faire la review du code

Les points à vérifier :
-   `Test` : Est-ce que les tests couvrent tous les cas de cette fonctionnalité ?
-   `Conventions`: Est-ce que les standards sont respectés ? Le naming clair ?
-   `Code style` : Est-ce que le code est facile à lire ? Est-ce qu’il doit être découpé ? Le formatage est géré par ESLint, ne perdez pas de temps à débattre dessus.
-   `Performance`: Est-ce qu’on fait plus d’appels que nécessaire ? Est-ce qu’on boucle sur toute la liste alors qu’on peut sortir avant? Est-ce qu’il y a une manière moins coûteuse de faire de faire cette vérification ?
-   `Sécurité`: Est-ce qu’il y a un potentiel problème de sécurité sur cette MR ?
-   `Implémentation`: Est-ce que c’est over engineered ? Est-ce que le code est trop spécifique ? Est-ce qu’on pouvait utiliser une autre brique ?

### 8. 😍 Soyez bienveillant et curieux !
-   ☺️ Faites des suggestions de manière positive
-   🙋 Posez des questions sur le code ! (surtout pour les new joiners 👶 ça vous rôdera pour l’exercice)
-   ✍️ Communiquer par écrit est plus difficile qu’à l’oral.
-   😉 C’est le code qui est critiqué, pas vos compétences ;)
  
### 9. ✅ Approuvez !
- Est-ce que la MR répond au comportement souhaité ?
- Est-ce que vous vous sentez capable de maintenir et déboguer ce nouveau code ?
```
⚠️ Tu devras très certainement retoucher à ce code dans le futur.
Assures-toi d'avoir bien compris le besoin métier et l'implémentation.
Plus de temps consommé aujourd'hui pour en perdre moins dans l'avenir 🚀
```

# Glossaire
- `commit`: nregistrement dans le dépôt Git de modifications apportées au code
- `merge request`: proposition de modification de code
- `new joiners`: un nouveau membre de l’équipe
- `MR`: merge request
- `push`: action d'envoyer un commit sur le serveur git

# Bibliographie
- https://google.github.io/eng-practices/review/
- https://medium.com/doctolib/how-to-perform-effective-code-reviews-like-a-human-being-952db46fd92e

