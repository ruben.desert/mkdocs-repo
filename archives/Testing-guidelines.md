# Testing guidelines

## Afficher des joli diffs

Code source d'exemple :

```typescript
import chai from 'chai'
import sinon from 'sinon'
import sinonChai from 'sinon-chai'

chai.use(sinonChai)

const object1 = {
  field_1: 'value 1',
  field_2: 'value 2',
  field_3: 'value 3',
  field_4: 'value 4',
  field_5: 'value 5',
  field_6: 'value 6',
  field_7: 'value 7',
  field_8: 'value 8',
  field_9: 'value 9',
  field_10: 'value 10',
  field_11: 'value 11',
  field_12: 'value 12',
  field_13: 'value 13',
  field_14: 'value 14',
  field_15: 'value 15',
  field_16: 'value 16',
  field_17: 'value 17',
  field_18: 'value 18',
  field_19: 'value 19',
  field_20: 'value 20'
}
const object2 = {
  field_1: 'value 1',
  field_2: 'value 2',
  field_3: 'value 3',
  field_4: 'value 4',
  field_5: 'value 5',
  field_6: 'value 6',
  field_7: 'value 7',
  field_8: 'value 8',
  field_9: 'value 9',
  field_10: 'value 10 bis',
  field_11: 'value 11',
  field_12: 'value 12',
  field_13: 'value 13',
  field_14: 'value 14',
  field_15: 'value 15',
  field_16: 'value 16',
  field_17: 'value 17',
  field_18: 'value 18',
  field_19: 'value 19',
  field_20: 'value 20'
}

describe('describe', async () => {
  it('assertion with chai : nice diff', async () => {
    try {
      chai.expect(object1).to.deep.equal(object2)
    } catch (error) {
      console.log(JSON.stringify(error, null, 2))
      throw error
    }
  })
  it('assertion with sinon : ugly diff', async () => {
    const fooMock = sinon.mock()
    fooMock(object1)
    try {
      sinon.assert.calledOnceWithExactly(fooMock, object2)
    } catch (error) {
      console.log(JSON.stringify(error, null, 2))
      throw error
    }
  })
  it('assertion with sinon-chai : ugly diff', async () => {
    const fooMock = sinon.mock()
    fooMock(object1)
    try {
      chai.expect(fooMock).to.have.been.calledOnceWithExactly(object2)
    } catch (error) {
      console.log(JSON.stringify(error, null, 2))
      throw error
    }
  })
})
```

### Assertions avec chai

#### Contenu de l'erreur

```json
{
  "name": "AssertionError",
  "message": "expected { Object (field_1, field_2, ...) } to equal { Object (field_1, field_2, ...) }",
  "showDiff": true,
  "actual": {
    "field_1": "value 1",
    "field_2": "value 2",
    "field_3": "value 3",
    "field_4": "value 4",
    "field_5": "value 5",
    "field_6": "value 6",
    "field_7": "value 7",
    "field_8": "value 8",
    "field_9": "value 9",
    "field_10": "value 10",
    "field_11": "value 11",
    "field_12": "value 12",
    "field_13": "value 13",
    "field_14": "value 14",
    "field_15": "value 15",
    "field_16": "value 16",
    "field_17": "value 17",
    "field_18": "value 18",
    "field_19": "value 19",
    "field_20": "value 20"
  },
  "expected": {
    "field_1": "value 1",
    "field_2": "value 2",
    "field_3": "value 3",
    "field_4": "value 4",
    "field_5": "value 5",
    "field_6": "value 6",
    "field_7": "value 7",
    "field_8": "value 8",
    "field_9": "value 9",
    "field_10": "value 10 bis",
    "field_11": "value 11",
    "field_12": "value 12",
    "field_13": "value 13",
    "field_14": "value 14",
    "field_15": "value 15",
    "field_16": "value 16",
    "field_17": "value 17",
    "field_18": "value 18",
    "field_19": "value 19",
    "field_20": "value 20"
  },
  "operator": "deepStrictEqual",
  "stack": "AssertionError: expected { Object (field_1, field_2, ...) } to equal { Object (field_1, field_2, ...) }\n    at Context.<anonymous> (/home/michel/tech/falco-api/test/app/quotes/diff.test.ts:59:31)\n    at callFn (/home/michel/tech/falco-api/node_modules/mocha/lib/runnable.js:374:21)\n    at Test.Runnable.run (/home/michel/tech/falco-api/node_modules/mocha/lib/runnable.js:361:7)\n    at Runner.runTest (/home/michel/tech/falco-api/node_modules/mocha/lib/runner.js:619:10)\n    at /home/michel/tech/falco-api/node_modules/mocha/lib/runner.js:745:12\n    at next (/home/michel/tech/falco-api/node_modules/mocha/lib/runner.js:536:14)\n    at /home/michel/tech/falco-api/node_modules/mocha/lib/runner.js:546:7\n    at next (/home/michel/tech/falco-api/node_modules/mocha/lib/runner.js:448:14)\n    at Immediate._onImmediate (/home/michel/tech/falco-api/node_modules/mocha/lib/runner.js:514:5)\n    at processImmediate (internal/timers.js:462:21)"
}
```

#### Rendu au terminal

![](images/ovJlkTZ.png)


#### Rendu avec Webstorm

![](images/9rF0VOE.png)

![](images/eNf2KIA.png)

### Assertions avec chai

#### Contenu de l'erreur

```json
{
  "name": "AssertError"
}
```

#### Rendu au terminal

![](images/tXSxewc.png)

#### Rendu avec Webstorm

![](images/3TyxwAf.png)

### Assertions avec sinon-chai

#### Contenu de l'erreur

```json
{
  "name": "AssertionError",
  "message": "expected Expectation met: Anonymous mock('[...]') once to have been called exactly once with exact arguments {\n  field_1: 'value 1',\n  field_2: 'value 2',\n  field_3: 'value 3',\n  field_4: 'value 4',\n  field_5: 'value 5',\n  field_6: 'value 6',\n  field_7: 'value 7',\n  field_8: 'value 8',\n  field_9: 'value 9',\n  field_10: 'value 10 bis',\n  field_11: 'value 11',\n  field_12: 'value 12',\n  field_13: 'value 13',\n  field_14: 'value 14',\n  field_15: 'value 15',\n  field_16: 'value 16',\n  field_17: 'value 17',\n  field_18: 'value 18',\n  field_19: 'value 19',\n  field_20: 'value 20'\n}\n{\n  field_1: 'value 1',\n  field_2: 'value 2',\n  field_3: 'value 3',\n  field_4: 'value 4',\n  field_5: 'value 5',\n  field_6: 'value 6',\n  field_7: 'value 7',\n  field_8: 'value 8',\n  field_9: 'value 9',\n\u001b[31m  field_10: 'value 10',\n\u001b[0m\u001b[32m  field_10: 'value 10 bis',\n\u001b[0m  field_11: 'value 11',\n  field_12: 'value 12',\n  field_13: 'value 13',\n  field_14: 'value 14',\n  field_15: 'value 15',\n  field_16: 'value 16',\n  field_17: 'value 17',\n  field_18: 'value 18',\n  field_19: 'value 19',\n  field_20: 'value 20'\n}",
  "showDiff": false,
  "stack": "AssertionError: expected Expectation met: Anonymous mock('[...]') once to have been called exactly once with exact arguments {\n  field_1: 'value 1',\n  field_2: 'value 2',\n  field_3: 'value 3',\n  field_4: 'value 4',\n  field_5: 'value 5',\n  field_6: 'value 6',\n  field_7: 'value 7',\n  field_8: 'value 8',\n  field_9: 'value 9',\n  field_10: 'value 10 bis',\n  field_11: 'value 11',\n  field_12: 'value 12',\n  field_13: 'value 13',\n  field_14: 'value 14',\n  field_15: 'value 15',\n  field_16: 'value 16',\n  field_17: 'value 17',\n  field_18: 'value 18',\n  field_19: 'value 19',\n  field_20: 'value 20'\n}\n{\n  field_1: 'value 1',\n  field_2: 'value 2',\n  field_3: 'value 3',\n  field_4: 'value 4',\n  field_5: 'value 5',\n  field_6: 'value 6',\n  field_7: 'value 7',\n  field_8: 'value 8',\n  field_9: 'value 9',\n\u001b[31m  field_10: 'value 10',\n\u001b[0m\u001b[32m  field_10: 'value 10 bis',\n\u001b[0m  field_11: 'value 11',\n  field_12: 'value 12',\n  field_13: 'value 13',\n  field_14: 'value 14',\n  field_15: 'value 15',\n  field_16: 'value 16',\n  field_17: 'value 17',\n  field_18: 'value 18',\n  field_19: 'value 19',\n  field_20: 'value 20'\n}\n    at Context.<anonymous> (/home/michel/tech/falco-api/test/app/quotes/diff.test.ts:79:41)\n    at callFn (/home/michel/tech/falco-api/node_modules/mocha/lib/runnable.js:374:21)\n    at Test.Runnable.run (/home/michel/tech/falco-api/node_modules/mocha/lib/runnable.js:361:7)\n    at Runner.runTest (/home/michel/tech/falco-api/node_modules/mocha/lib/runner.js:619:10)\n    at /home/michel/tech/falco-api/node_modules/mocha/lib/runner.js:745:12\n    at next (/home/michel/tech/falco-api/node_modules/mocha/lib/runner.js:536:14)\n    at /home/michel/tech/falco-api/node_modules/mocha/lib/runner.js:546:7\n    at next (/home/michel/tech/falco-api/node_modules/mocha/lib/runner.js:448:14)\n    at Immediate._onImmediate (/home/michel/tech/falco-api/node_modules/mocha/lib/runner.js:514:5)\n    at processImmediate (internal/timers.js:462:21)"
}
```

#### Rendu au terminal

![](images/5qzeXGA.png)

#### Rendu avec Webstorm

![](images/ttgxYZk.png)

### Conclusions

Seul chai throw des erreurs qui comportent les champs `actual` et `expected`. Ces champs peuvent être compris par l'éditeur ou par mocha pour afficher de joli diffs.

Il est donc recommandé d'utiliser chai plutôt que sinon/sinon-chai pour les asertions comparant 2 objects complexes.

#### Exemple

A éviter : utilisation de sinon-chai pour un stub et pour les assertions

```typescript
const fooMock = sinon.mock()
fooMock(object1)
chai.expect(fooMock).to.have.been.calledOnceWithExactly(object2)
```

A préférer : utilisation de sinon pour un stub mais comparaison d'objets avec chai

```typescript
const fooMock = sinon.mock()
fooMock(object1)
chai.expect(fooMock).to.have.been.calledOnce // sinon-chai
chai.expect(fooMock.firstCall.firstArg).to.deep.equal(object2) // chai
```

> NB - dans la codebase falco-api, il n'y a pas besoin de préciser `chai.expect(...)` car `expect(...)` suffit. En effet, le module `test/test-utils.ts` expose directement `chai.expect` :

```typescript
const expect = chai.expect
```

## De l'usage abusif de `withArgs`

`withArgs` permet de spécifier plusieurs valeurs de retours, pour le cas où un test double est appelé plusieurs fois avec des arguments différents.

Dans le cas où le test double n'est appelé qu'une seule fois, il n'y a pas de raison d'utiliser `withArgs`. S'il faut vérifier que le test double a été appelé avec les arguments attendus, il vaut mieux ajouter une assertion.

En effet, un test double configuré avec `withArgs` qui n'est pas appelé avec les bons arguments renvoie `undefined` au lieu de lever une exception. Le test peut même passer dans certains cas.

```typescript
// import { expect, sinon } from '../../test-utils'

import chai from 'chai'
import sinon from 'sinon'

const sinonChai = require('sinon-chai')
const chaiAsPromised = require('chai-as-promised')

chai.use(sinonChai)
chai.use(chaiAsPromised)

class Dependency {
  static async processStr (arg: string): Promise<string> {
    return Promise.resolve(arg.toUpperCase())
  }
}

async function systemUnderTest1 (left: string, right: string): Promise<string> {
  const firstResult = await Dependency.processStr(left)
  const secondResult = await Dependency.processStr(right)
  return `${firstResult}-${secondResult}`
}

async function systemUnderTest2 (password: string): Promise<string> {
  const hashedPassword = 'prefix' + await Dependency.processStr(password + password)

  if (hashedPassword.length < 15) {
    return 'Password is not secure'
  } else {
    return 'Password OK'
  }
}

describe('describe', async () => {
  const stubbedFunction = sinon.stub(Dependency, 'processStr')

  afterEach(async () => {
    stubbedFunction.reset()
  })
  it('dependency is called twice', async () => {
    // Given
    stubbedFunction.withArgs('hi').resolves('Hi')
    stubbedFunction.withArgs('hey').resolves('Hey')

    // When
    const result = await systemUnderTest1('hi', 'hey')

    // Then
    chai.expect(result).to.equal('Hi-Hey')
  })
  it('dependency is called once, withArgs', async () => {
    // Given
    stubbedFunction.withArgs('abcdefghi123456').resolves('Abcdefghi123456')

    // When
    const result = await systemUnderTest2('abcdefghi123456')

    // Then
    chai.expect(result).to.equal('Password OK')
  })
  it('dependency is called once, no withArgs', async () => {
    // Given
    stubbedFunction.resolves('Abcdefghi123456')

    // When
    const result = await systemUnderTest2('abcdefghi123456')

    // Then
    chai.expect(result).to.equal('Password OK')
    chai.expect(stubbedFunction).to.have.been.calledOnceWithExactly('abcdefghi123456')
  })
})
```

Le premier test montre un exemple d'utilisation légitime de `withArgs`, car le test double est appelé plusieurs fois.

Le deuxième test passe alors qu'il devrait échouer.

Le troisième test échoue car c'est bien une assertion qui est utilisée pour vérifier le paramètre envoyés au test double.

```text
  describe
    ✓ dependency is called twice
    ✓ dependency is called once, withArgs
    1) dependency is called once, no withArgs


  2 passing (19ms)
  1 failing

  1) describe
       dependency is called once, no withArgs:
     AssertionError: expected processStr to have been called exactly once with exact arguments 'abcdefghi123456'
'"abcdefghi123456abcdefghi123456"' '"abcdefghi123456"' 
      at Context.<anonymous> (test/app/quotes/diff.test.ts:70:47)
```

## Mise en oeuvre

Pas de refactoring big-bang mais appliquer ces guidelines au fur et à mesure.
