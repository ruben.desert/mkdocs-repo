# Migrations

## Réversibilité

En principe, chaque migration doit être réversible.

Néanmoins, il existe des cas particuliers pour lesquels il est difficile ou peu utile de rendre la migration réversible.

> Par exemple, la migration `up` supprime une colonne. Pour rendre la migration réversible, elle devra conserver les informations de la colonne dans une nouvelle colonne "temporaire" qui n'aura d'autres usages que de permettre la migration `down`.

Si la question se pose pour une migration donnée, le sujet est abordé en grooming.
