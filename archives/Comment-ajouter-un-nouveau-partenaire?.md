Les informations liées au produit et parcours du nouveau partenaire sont alimentés par l'équipe BizDev et stockés dans un fichier 'provisionning':
- [Fichier provisionning STUDYO](https://www.dropbox.com/s/3n1yeiyj0mx8vh0/Provisionning%20STUDYO%20-%20V2%20-%2020200831.xlsx?dl=0)
- [Fichier provisionning ESSCA](https://www.dropbox.com/s/8rhum8aavfp033x/Provisionning%20ESSCA%20-%20V2%20-%2020200831.xlsx?dl=0)
- [Fichier provisionning DEMO-STUDENT](https://www.dropbox.com/s/g7gf41yq8buz0oy/Provisionning%20DEMO-STUDENT%20-%20V2%20-%2020200831.xlsx?dl=0)
- [Fichier provisionning E-MOBILIA ALTERNANT](https://www.dropbox.com/s/w634i05in8t9dqy/Provisionning%20E-MOBILIA%20-%20alternants.xlsx?dl=0)
- [Fichier provisionning E-MOBILIA GENERIQUE](https://www.dropbox.com/s/p9snpgcs3orywoe/Provisionning%20E-MOBILIA%20-%20g%C3%A9n%C3%A9rique.xlsx?dl=0)
- [Fichier provisionning ESTUDENT](https://www.dropbox.com/s/w44vgojq91a9xix/Provisionning%20ESTUDENT.xlsx?dl=0)
- [Fichier provisionning GREYSTAR](https://www.dropbox.com/s/nzjudx2ma9ffcqp/Provisionning%20GREYSTAR.xlsx?dl=0)
- [Fichier provisionning DEMO](https://www.dropbox.com/s/uvs4ok0444xctax/Provisionning%20DEMO.xlsx?dl=0)

Le numéro de contrat du client est préfixé par un trigramme unique par partenaire.

Les fichiers de provisionning prévoient :
| partner | trigram |
| ------ | ------ |
| studyo | `STU` |
| essca | `ESS` |
| demo-student | `DST` |
| e-mobilia-alternant | `EMA` |
| e-mobilia-generique | `EMO` |
| estudent | `EST` |
| greystar | `GRE` |
| demo | `DEM` |