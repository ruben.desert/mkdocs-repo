# Générer manuellemement une attestation

## Besoin

Un client a besoin d'une attestation autre que celle générée automatiquement. Par exemple, il faut renseigner un complément d'adresse.

## Etapes

* Prendre l'attestation actuellement générée
* La décompresser avec `pdftk uncompress`

```sh
pdftk Appenin_Attestation_assurance_habitation_SDC658957473.pdf output Appenin_Attestation_assurance_habitation_SDC658957473.pdf.uncompressed uncompress
```

* Aller ajouter les compléments dans le texte (en espérant que tout tienne bien sur une ligne, car pas de reflow automatique). Attention à utiliser un éditeur qui préserve les caractères binaires à l'enregistrement (par exemple vim mais pas nano ni vscode).

```txt
(Barbara Avila)Tj
ET
EMC
/Span <</Lang (fr-FR)/MCID 10 >>BDC
BT
10 0 0 10 141.7323 420.4376 Tm
(59 Rue Beaubourg, bât 1, 2e étage, porte gauche)Tj
ET
EMC
```

* Recompresser avec `pdftk compress`

```sh
pdftk Appenin_Attestation_assurance_habitation_SDC658957473.pdf.uncompressed output Appenin_Attestation_assurance_habitation_SDC658957473_bis.pdf compress
```
