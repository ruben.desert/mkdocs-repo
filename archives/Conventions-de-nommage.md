# Conventions de nommage

## Fonctions `buildXxx` / `saveXxx` / `createXxx` / `updateXxx`

Dans le domaine, les fonctions `buildXxx` sont responsables de construire un nouvel objet, mais ne persistent pas l'objet créé par le biais du repository.

Au contraire, les fonctions `saveXxx` prennent en paramètre un objet déjà construit et gèrent la persistance via le repository.

Les fonctions `createXxx` construisent et prennent en charge la persistance.

Les fonctions `updateXxx` mettent à jour un objet et prennent en charge ou pas la persistance (à faire évoluer ?).

## Nommage des dates

Les dates `XxxAt` représentent des datetimes (date + heure) tandis que les date `XxxDate` ne représentent qu'une date.

## Objets mutables / immutables

Quand une fonction effectue un traitement sur un objet, on préfère généralement renvoyer un nouvel objet qui est la copie modifiée de l'original. Ceci afin de privilégier l'immutabilité qui est réputée plus robuste et maintenable.

Si par exception une fonction modifie un objet en place, alors elle ne doit rien renvoyer, elle est de préférence utilisée uniquement de manière interne, et elle peut prendre un nom de la forme `_applyXxx`.
