# Vocabulaire métier

## Default cap

Le `default cap` est le montant plafond utilisé par défaut pour les garanties qui n'ont pas de montant plafond spécifique.

Par exemple, le plafond de dédommagement de la responsabilité civile corporelle est fixé de manière spécifique (ce montant vaut en général plusieurs millions), au contraire de la garantie vol.

## Valuables cap

Le `valuables cap` est le montant plafond des objets de valeur couverts par la police d'assurance.

## Start date

La `start date` est la date d'émission de la prime. Cette date a une signification comptable.

## Term start date / term end date

La `term start date` (resp. `term end date`) est la date de début (resp. de fin) de la période de couverture en cours. Ces dates ont une signification assurantielle.

## Signature date

La `signature date` est la date de signature du contrat régissant la police en cours.

## Payment date

La `payment date` est la date de paiement de la prime. Cette date a une signification financière.

## Garantie technique vs garantie commerciale

Voir https://www.dropbox.com/scl/fi/yzz8y923v7p1isz238ckr/garanties-Appenin.xlsx.xlsx pour une liste des garanties proposées par Appenin.

L'équipe actuariale élabore des tarifs pour les garanties *techniques*. Un code à 6 lettres est attribué à chaque garantie technique.

Actuellement, les garanties suivantes sont couvertes par les deux produits :

* ASSIST : Assistance
* BDVIMM : Bris de vitre immobilier
* CLIXXX : Climatiques indifférenciée hors inondations
* DDEAUX : Dégats des eaux
* INCEND : Incendie
* RCXXXX : Responsabilité civile indifférenciée
* VOLXXX : Vol indifférencié

La garantie RECDEF (Recours et défense) n'est proposée qu'aux étudiants tandis que la garantie CLIINO (Evénements climatiques inondations hors catnat) n'est proposée que par le produit généraliste.

Les garanties présentées aux clients, dites *commerciales*, correspondent plus ou moins aux contours des garanties techniques. Un code commençant par "AC" (pour "Advertising Cover") est attribué à chaque garantie commerciale. Appenin propose actuellement les garanties commerciales suivantes :

* ACDDE : dégâts des eaux
* ACINCEX : incendie et explosion
* ACVOL : vol et vandalisme
* ACASSHE : assistance hébergement
* ACDEFJU : défense juridique
* ACRC : responsabilité civile

Il n'y a pas de correspondance simple entre garanties commerciales et garanties juridiques. Ainsi, les garanties commerciales entre le produit étudiant et le produit généraliste sont les même alors que les garanties techniques sont différentes.

Une garantie technique peut exister pour des raisons réglementaires ou comptables sans correspondre à une garantie commerciale. C'est le cas de la garantie technique `CATNAT` (catastrophes naturelles) couvre différents types de dégâts (dégâts des eaux, responsabilité civile...) provoqués par une catastrophe naturelle.

Bonus : les garanties *indifférenciées* sont des regroupement de plusieurs garanties techniques. Elles sont utilisées à des fins comptables ou pour le reporting Covéa. Par exemple, `RCXXXX` est une garantie indifférenciée qui regroupe `RCLMAT` (responsabilité civile matérielle) et `RCLCOR` (responsabilité civile corporelle). Idem pour `CLIXXX` = `CLITEM` (tempête) + `CLINEI` (neige) + `CLIGRE` (grêle) + `CLIGEL` (gel).
