# Charte technique

Cette charte définie l'ensemble de consignes techniques, nos workflows de fonctionnement et autres.

## Langue adoptée Pour la rédaction des documents

- **Documentation Technique** : En anglais

On dit de la documentation technique tout ce qui va est attrait à de la technique pure et dure . C'est le cas par exemple des docs d'api, d'explication d'architecture projet, explication technique d'une fonctionnalité, ou encore une doc d'onboarding et etc.

- **Documentation Fonctionnelle** : En Français

On dit de la documentation fonctionnelle tout ce qui est attrait à la partie gestion projet. Les US, Epics, et tickets sont considéré comme de la documentation fonctionnelle.

### Stratégie adoptée pour la gestion du workflow git

- Commit -> En anglais
- MR titre et description -> En anglais
- MR comments -> Libre (choisir la langue permettant de mieux expliquer le problème)

## Passage d'un ticket de `review` à `dev`

Un ticket peut passer de la colonne `review` à la colonne `dev` dans certains cas.

- C'est le dev qui gère son ticket : c'est en principe lui qui repasse le ticket en dev (sauf cas particulier, comme pipeline cassée)
- La règle générale est de laisser en review. Mais si le dev ne peux pas prendre en compte les retours en 1/2 journée, il repasse le ticket en dev ; autre exemple : pipeline cassée

## Review de la documentation

Chaque modification apportée au repository `falco-docs` nécessite un approval. C'est par exemple suffisant pour les modifications suivantes :

- corrections mineures
- tutoriels
- compte rendu d'un sujet discuté en point tech

Les modifications les plus importantes doivent être approuvées en principe par l'ensemble des membres disponibles. C'est par exemple le cas :

- des guidelines de développement
- des règles de fonctionnement de l'équipe

Pour ces modifications importantes, la MR doit rester deux jours pleins en phase de review et être approuvée par au moins un membre de l'équipe technique.
