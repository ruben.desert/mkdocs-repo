### 1. URL Simple

Aujourd'hui lorsque nous créons un nouveau Partenaire, nous lui attribuons une **URL** dédiée :
* _Exemple pour le Partenaire `demo-student`_
  * https://staging.assurance.appenin.fr/demo-student

### 2. URL avec Apporteur

Suite à une demande propre aux besoins de `simply-move`, gestionnaire de conciergeries qui souhaite pouvoir proposer le produit d'assurance appenin **APP978** à tout public au travers d'apporteurs. Nous avons développé un système d'**URL avec Apporteur**. 

Cela fonctionne sur le  principe d'enregistrement de "mots-clés" qui permettent de laisser la main au Partenaire afin qu'il puisse créer autant d'**URL avec Apporteur** qu'il le désire.

Ce qui permet côté BizDev, de tracer ces apporteurs amenés par les Partenaires, pour ensuite faire du reporting, commissionnement, etc.

- Le 25 juin 2021, nous avons **6** "mots-clés" (`origin`) :

| mots-clés           |
|---------------------|
| origin              | 
| origine             |
| agent               | 
| agence              |
| residence           | 
| conciergerie        |


NB: _Ces "mots-clés" sont prédéfinis nous évite d'avoir recours à des caractères spéciaux (`=` `?`) dans l'URL (URL beaucoup plus propre)._

---
De fait, maintenant pour l'ensemble des Partenaires appenin, tout ce qui est saisi en mode **URL avec Apporteur** est enregistré en base comme un parcours classique du Partenaire. Il est ainsi possible de mettre n'importe quoi derrière ce "mot-clé". 
Par exemple si nous souhaitons tracer les clients "apportés" par les agences ERA dans le 44 sur le Partenaire `estudent` cela est possible simplement en saisissant l'URL suivante :

  * https://staging.assurance.appenin.fr/estudent/agence/ERA44
  * https://staging.assurance.appenin.fr/estudent/agence/ERA-44
  * https://staging.assurance.appenin.fr/estudent/agence/ERAdu44
  * https://staging.assurance.appenin.fr/estudent/agence/ERA44-place-graslin
  * Etc.

Concernant les enregistrements, il est important de savoir que seule ce qui est saisi suite au mot-clé est visible et enregistré en table. Tous les mots clés sont considérés comme **identiques**, ainsi deux **URL avec Apporteur** différentes :
* https://staging.assurance.appenin.fr/estudent/agence/ERA44

ou
* https://staging.assurance.appenin.fr/estudent/conciergerie/ERA44

  * seront enregistrées de la même façon dans le reporting [Metabase](https://data.appenin.fr/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7ImRhdGFiYXNlIjozLCJxdWVyeSI6eyJzb3VyY2UtdGFibGUiOjQ3LCJmaWVsZHMiOltbImZpZWxkLWlkIiwzOTZdLFsiZmllbGQtaWQiLDQwNF0sWyJmaWVsZC1pZCIsNDA1XSxbImZpZWxkLWlkIiwzOTddLFsiZGF0ZXRpbWUtZmllbGQiLFsiZmllbGQtaWQiLDQwMl0sImRlZmF1bHQiXSxbImRhdGV0aW1lLWZpZWxkIixbImZpZWxkLWlkIiwzOTldLCJkZWZhdWx0Il0sWyJkYXRldGltZS1maWVsZCIsWyJmaWVsZC1pZCIsNDAzXSwiZGVmYXVsdCJdLFsiZGF0ZXRpbWUtZmllbGQiLFsiZmllbGQtaWQiLDQxM10sImRlZmF1bHQiXSxbImRhdGV0aW1lLWZpZWxkIixbImZpZWxkLWlkIiw0MDBdLCJkZWZhdWx0Il0sWyJkYXRldGltZS1maWVsZCIsWyJmaWVsZC1pZCIsNDA2XSwiZGVmYXVsdCJdLFsiZmllbGQtaWQiLDQwMV0sWyJmaWVsZC1pZCIsMzk1XSxbImRhdGV0aW1lLWZpZWxkIixbImZpZWxkLWlkIiw0MDldLCJkZWZhdWx0Il0sWyJkYXRldGltZS1maWVsZCIsWyJmaWVsZC1pZCIsNDE0XSwiZGVmYXVsdCJdLFsiZmllbGQtaWQiLDQxMV0sWyJkYXRldGltZS1maWVsZCIsWyJmaWVsZC1pZCIsNTgxXSwiZGVmYXVsdCJdLFsiZmllbGQtaWQiLDU3MV0sWyJmaWVsZC1pZCIsNDA4XSxbImZpZWxkLWlkIiw1NjZdLFsiZGF0ZXRpbWUtZmllbGQiLFsiZmllbGQtaWQiLDU2NV0sImRlZmF1bHQiXSxbImZpZWxkLWlkIiw0MTBdXSwiZmlsdGVyIjpbImFuZCIsWyI9IixbImZpZWxkLWlkIiw0MDRdLCJlc3R1ZGVudCJdLFsibm90LWVtcHR5IixbImZpZWxkLWlkIiw1NzFdXV19LCJ0eXBlIjoicXVlcnkifSwiZGlzcGxheSI6InRhYmxlIiwidmlzdWFsaXphdGlvbl9zZXR0aW5ncyI6eyJ0YWJsZS5jb2x1bW5zIjpbeyJuYW1lIjoiaWQiLCJmaWVsZFJlZiI6WyJmaWVsZC1pZCIsMzk2XSwiZW5hYmxlZCI6dHJ1ZX0seyJuYW1lIjoicGFydG5lcl9jb2RlIiwiZmllbGRSZWYiOlsiZmllbGQtaWQiLDQwNF0sImVuYWJsZWQiOnRydWV9LHsibmFtZSI6Im9yaWdpbiIsImZpZWxkUmVmIjpbImZpZWxkLWlkIiw1NzFdLCJlbmFibGVkIjp0cnVlfSx7Im5hbWUiOiJwcmVtaXVtIiwiZmllbGRSZWYiOlsiZmllbGQtaWQiLDQwNV0sImVuYWJsZWQiOnRydWV9LHsibmFtZSI6Im5iX21vbnRoc19kdWUiLCJmaWVsZFJlZiI6WyJmaWVsZC1pZCIsMzk3XSwiZW5hYmxlZCI6dHJ1ZX0seyJuYW1lIjoic3Vic2NyaXB0aW9uX2RhdGUiLCJmaWVsZFJlZiI6WyJkYXRldGltZS1maWVsZCIsWyJmaWVsZC1pZCIsNDAyXSwiZGVmYXVsdCJdLCJlbmFibGVkIjp0cnVlfSx7Im5hbWUiOiJzdGFydF9kYXRlIiwiZmllbGRSZWYiOlsiZGF0ZXRpbWUtZmllbGQiLFsiZmllbGQtaWQiLDM5OV0sImRlZmF1bHQiXSwiZW5hYmxlZCI6dHJ1ZX0seyJuYW1lIjoidGVybV9zdGFydF9kYXRlIiwiZmllbGRSZWYiOlsiZGF0ZXRpbWUtZmllbGQiLFsiZmllbGQtaWQiLDQwM10sImRlZmF1bHQiXSwiZW5hYmxlZCI6dHJ1ZX0seyJuYW1lIjoidGVybV9lbmRfZGF0ZSIsImZpZWxkUmVmIjpbImRhdGV0aW1lLWZpZWxkIixbImZpZWxkLWlkIiw0MTNdLCJkZWZhdWx0Il0sImVuYWJsZWQiOnRydWV9LHsibmFtZSI6InNpZ25hdHVyZV9kYXRlIiwiZmllbGRSZWYiOlsiZGF0ZXRpbWUtZmllbGQiLFsiZmllbGQtaWQiLDQwMF0sImRlZmF1bHQiXSwiZW5hYmxlZCI6dHJ1ZX0seyJuYW1lIjoicGF5bWVudF9kYXRlIiwiZmllbGRSZWYiOlsiZGF0ZXRpbWUtZmllbGQiLFsiZmllbGQtaWQiLDQwNl0sImRlZmF1bHQiXSwiZW5hYmxlZCI6dHJ1ZX0seyJuYW1lIjoicG9saWN5X2luc3VyYW5jZV9pZCIsImZpZWxkUmVmIjpbImZpZWxkLWlkIiw0MDFdLCJlbmFibGVkIjp0cnVlfSx7Im5hbWUiOiJwb2xpY3lfcmlza19pZCIsImZpZWxkUmVmIjpbImZpZWxkLWlkIiwzOTVdLCJlbmFibGVkIjp0cnVlfSx7Im5hbWUiOiJjcmVhdGVkX2F0IiwiZmllbGRSZWYiOlsiZGF0ZXRpbWUtZmllbGQiLFsiZmllbGQtaWQiLDQwOV0sImRlZmF1bHQiXSwiZW5hYmxlZCI6dHJ1ZX0seyJuYW1lIjoidXBkYXRlZF9hdCIsImZpZWxkUmVmIjpbImRhdGV0aW1lLWZpZWxkIixbImZpZWxkLWlkIiw0MTRdLCJkZWZhdWx0Il0sImVuYWJsZWQiOnRydWV9LHsibmFtZSI6InN0YXR1cyIsImZpZWxkUmVmIjpbImZpZWxkLWlkIiw0MTFdLCJlbmFibGVkIjp0cnVlfSx7Im5hbWUiOiJlbWFpbF92YWxpZGF0ZWRfYXQiLCJmaWVsZFJlZiI6WyJkYXRldGltZS1maWVsZCIsWyJmaWVsZC1pZCIsNTgxXSwiZGVmYXVsdCJdLCJlbmFibGVkIjp0cnVlfSx7Im5hbWUiOiJwb2xpY3lfaG9sZGVyX2lkIiwiZmllbGRSZWYiOlsiZmllbGQtaWQiLDQwOF0sImVuYWJsZWQiOnRydWV9LHsibmFtZSI6InNwZWNpYWxfb3BlcmF0aW9uX2NvZGUiLCJmaWVsZFJlZiI6WyJmaWVsZC1pZCIsNTY2XSwiZW5hYmxlZCI6dHJ1ZX0seyJuYW1lIjoic3BlY2lhbF9vcGVyYXRpb25fY29kZV9hcHBsaWVkX2F0IiwiZmllbGRSZWYiOlsiZGF0ZXRpbWUtZmllbGQiLFsiZmllbGQtaWQiLDU2NV0sImRlZmF1bHQiXSwiZW5hYmxlZCI6dHJ1ZX0seyJuYW1lIjoicXVvdGVfaWQiLCJmaWVsZFJlZiI6WyJmaWVsZC1pZCIsNDEwXSwiZW5hYmxlZCI6dHJ1ZX1dfSwib3JpZ2luYWxfY2FyZF9pZCI6bnVsbH0=)
  
![Question____Metabase_2021-05-20_18-25-11](uploads/6a1fd6d35fef77825714e1049292a100/Question____Metabase_2021-05-20_18-25-11.png)

---
### Récapitulatif 

[+Ce qui fonctionne+]
* https://staging.assurance.appenin.fr/demo-student/agence/ERA44
* https://staging.assurance.appenin.fr/demo-student/conciergerie/ERA44
* https://staging.assurance.appenin.fr/demo-student/origine/ERA44

[-Ce qui ne fonctionnera pas-]   
* https://staging.assurance.appenin.fr/demo-student/ERA44

---

Nous pouvons ajouter de nouveaux "mots-clés" qui permettrons aux Partenaires de gérer des URL par région par exemple pour le mot `residence` pour obtenir une **URL avec Apporteur** du type :

* https://staging.assurance.appenin.fr/greystar/siege/Massy2
  * **_actuellement en erreur 404 puisque ce mot "siege" ne fait pas partie de la liste des "mots-clés"_**
