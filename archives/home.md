## Bienvenue sur le wiki Appenin

Appenin propose un parcours de souscription en ligne où le souscripteur procède à la signature de son contrat et au paiement. Il reçoit son attestation d’assurance et tous les documents contractuels immédiatement par email.

La signature se fait via **HelloSign** et le paiement via **Stripe**.

## Intégration

- Le parcours de souscription peut être hébergé sur le site Appenin. Les partenaires redirigent leurs clients vers une url dédiée.

- Le parcours peut également être intégré par notre API depuis la plateforme du partenaire (statut à l'ORIAS nécessaire). Vous pouvez retrouver ici la [documentation de l'API](https://staging.api.appenin.fr/documentation).

## Partenaires

- En Janvier 2021, Appenin compte 17 partenaires: **Essca**, **Studyo**, **E-Mobilia-alternant**, **Greystar** et **Estudent** (principalement des gestionnaires de résidences étudiantes) qui proposent un parcours et un produit spécifique aux étudiants ('produit APP658').

- E-Mobilia est une solution de déménagement pour particuliers. Appenin a développé un premier parcours pour leurs clients alternants. Un deuxième parcours générique est en cours de développement ('produit APP978')

- Pour accompagner l'équipe Bizdev, l’équipe a développé un [parcours de démo pour le produit étudiant](https://assurance.appenin.fr/demo-student) et un [parcours de démo pour le produit locataire d'appartement](https://assurance.appenin.fr/demo) (lancement du MVP en janvier 2021).


[Comment ajouter un nouveau partenaire ?](https://gitlab.com/groups/appenin/-/wikis/Comment-ajouter-un-nouveau-partenaire%3F)

- Le 14 juin 2021, nous avons **20** partenaires et la possibilité de créer des Partenaires de DEMO Personnalisables :


## Environnements de tests :

PARTENAIRES DE **DEMO**

1. https://staging.assurance.appenin.fr/demo
2. https://staging.assurance.appenin.fr/demo-mrh
3. https://staging.assurance.appenin.fr/demo-student
---

**PARTENAIRES**

4. https://staging.assurance.appenin.fr/open
5. https://staging.assurance.appenin.fr/essca
6. https://staging.assurance.appenin.fr/novare
7. https://staging.assurance.appenin.fr/studyo
8. https://staging.assurance.appenin.fr/azoulay
9. https://staging.assurance.appenin.fr/bde-icn
10. https://staging.assurance.appenin.fr/greystar
11. https://staging.assurance.appenin.fr/estudent
12. https://staging.assurance.appenin.fr/studcorp
13. https://staging.assurance.appenin.fr/e-mobilia
14. https://staging.assurance.appenin.fr/simply-move
15. https://staging.assurance.appenin.fr/whoo-bacalan
16. https://staging.assurance.appenin.fr/groupe-pichet
17. https://staging.assurance.appenin.fr/novare-etudiant
18. https://staging.assurance.appenin.fr/azoulay-etudiant
19. https://staging.assurance.appenin.fr/e-mobilia-alternant
20. https://staging.assurance.appenin.fr/easystudent-rennes
---

Exemple de PARTENAIRE **URL avec APPORTEUR**

* https://staging.assurance.appenin.fr/simply-move/conciergerie/Recette

---

Exemple de PARTENAIRE de **DEMO PERSONNALISÉ**

* https://staging.assurance.appenin.fr/demo?ct=Nh34qG

---

Utilisation de la **Carte bleue de test** valable sur l'environnement staging : `4242 4242 4242 4242`