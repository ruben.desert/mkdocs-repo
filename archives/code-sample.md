```js
export const dbTestUtils = (function () {
    let sequelize: Sequelize
    return {
        initDB: async () => {
            sequelize = await initSequelize(config) // (1)
        },
        closeDB: async () => {
            if (sequelize) await sequelize.close()
        }
    }
}())
```
1. ...
