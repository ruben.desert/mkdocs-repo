# Quote

A quote is like a blank sheet of paper,
it's the first step to create a new policy for your users.

## Workflow :

Before creating a policy for your user, you will need to create a quote.
A quote contains some informations about the **insured person**, 
the **location of insured property** or the **premium**.

## Business rules :


