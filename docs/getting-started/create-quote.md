# Create a quote

In this section we will show you how to create and configurate a new quote.

## Introduction

## Integrations
| Method      | Description                          | URL        |
| ----------- | ------------------------------------ | ---------- |
| `POST`       | :material-check-all: Create resource | /v1/quotes|

**Payload sample :** 
```json
{
  "code": "demo",
  "risk": {
    "property": {
      "room_count": 3,
      "address": "59 Rue Beaubourg",
      "postal_code": "75003",
      "type": "FLAT",
      "city": "Paris",
      "occupancy": "TENANT"
    },
    "person": {
      "firstname": "Ruben",
      "lastname": "Faro"
    },
    "roommates": [
         {
        "firstname": "Ruben",
        "lastname": "Faro"
    }
    ]
  },
  "policy_holder": {
    "email": "Lyon.doe@gmail.com",
    "phone_number": "+33684205510",
    "firstname": "Lyon",
    "lastname": "Doe",
    "address": "Quelquepart",
    "postal_code":"97110",
    "city":"Paris"
  }
}
```
