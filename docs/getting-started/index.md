# Getting Started

The Falco API is organized around REST. Our API has predictable resource-oriented URLs, accepts form-encoded request bodies, returns JSON-encoded responses, and uses standard HTTP response codes, authentication, and verbs.

## Features

With Falco API you can :

- Create a new [quote](../ressources/quote.md) for your user
- Update this quote
- Transform a quote to a Policy

| Method      | Description                          |
| ----------- | ------------------------------------ |
| `GET`       | :material-check:     Fetch resource  |
| `PUT`       | :material-check-all: Update resource |
| `DELETE`    | :material-close:     Delete resource |


## Next steps

Learn how to get access to the Falco API so you can get started building.

Learn more about the different resources that we've built to help you get started.

Review our step-by-step tutorial on how to make your first policy.
